import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs';

import { UserModel } from '../../models/user.model';
import { UsersService } from 'src/app/services/users.service';

import Swal from 'sweetalert2';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  user: UserModel = new UserModel();

  constructor( private usersService: UsersService,
               private route: ActivatedRoute ) { }

  ngOnInit() {

    const idUser = this.route.snapshot.paramMap.get('idUser');

    if ( idUser !== 'nuevo' ) {

      this.usersService.getUser( idUser )
        .subscribe( (resp: UserModel ) => {
          this.user = resp;
          this.user.idUser = idUser;
        });
    }

  }

  guardar( form: NgForm ) {

    if ( form.invalid ) {
      console.log('Formulario no valido');
      return;
    }

    Swal.fire({
      title: 'Espere',
      icon: 'info',
      text: 'Guardando Informacion',
      allowOutsideClick: true
    });
    Swal.showLoading();

    let peticion: Observable<any>;

    if ( false ) {
      peticion = this.usersService.actualizarUser( this.user );
    } else {
      peticion = this.usersService.crearUser( this.user );
    }

    peticion.subscribe( resp => {
      Swal.fire({
        title: this.user.userName,
        text: 'Se actualizo corretamente',
        icon: 'success'
      });
    });
    // console.log(form);
    // console.log(this.heroe);
  }



}
