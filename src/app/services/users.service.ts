import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserModel } from '../models/user.model';
import { map, delay } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class UsersService {


  private url = 'http://localhost:5000/api';

  constructor( private http: HttpClient ) { }

  crearUser( user: UserModel ) {
    return this.http.post(`${ this.url }/user-create`, user)
            .pipe(
              map( (resp: any) => {
                user.idUser = resp.name;
                return user;
              })
            );
  }

  actualizarUser( user: UserModel ) {

    const userTemp = {
      ...user
    };

    delete userTemp.idUser;

    return this.http.put(`${ this.url }/user-update/${ user.idUser }`, userTemp);
  }

  getUser( idUser: string ) {
    return this.http.get(`${ this.url }/user/${ idUser }`);
  }

  getUsers() {
    return this.http.get(`${ this.url }/user`)
            .pipe(
              map( resp => this.crearArreglo(resp) ),
              delay(1500)
            );
  }

  borrarUser( idUser: string ) {
    return this.http.delete(`${ this.url }/user-delete/${ idUser }`);
  }

  private crearArreglo( usersObj: object ) {

    const users: UserModel[] = [];

    console.log(usersObj);

    if ( usersObj === null ) { return []; }

    Object.keys( usersObj ).forEach( key => {

      const user: UserModel = usersObj[key];
      user.idUser = key;

      users.push( user );

    });

    return users;
  }

}
